///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02d - Mouse `n Cat - EE 205 - Spr 2022
///
/// This C++ program will either:
///   1.  Print the command line arguments in reverse order
///   2.  or, if there are no command line arguments, print all of the
///       environment variables passed into the program
///
/// @file    mouseNcat.cpp
/// @version 1.0 - Initial version
///
/// Compile: $ g++ -o mouseNcat mouseNcat.cpp
///
/// Usage:  mouseNcat [param1] [param2] ...
///
/// Example:
///   With a command line parameter:
///   $ ./mouseNcat I am Sam
///   Sam
///   am
///   I
///   $
///
///   Without a command line parameter:
///   $ ./mouseNcat
///   SHELL=/bin/bash
///   ...
///   SSH_TTY=/dev/pts/0
///
/// @author  Jordan Cortado <jcortado@hawaii.edu>
/// @date    26_Jan_2022
///////////////////////////////////////////////////////////////////////////////


#include <iostream>
#include <cstdlib>

// Note the new main() parameter:  envp
int main( int argc, char* argv[], char* envp[] ) { 

   if(argc >=  2){
      
      for (int i=argc-1; i >= 1 ; i--){

          std::cout << argv[i] << std::endl;

      }
      std::exit( 0 );
   }
   else{

      for (int k = 0; envp[k] != NULL ; k++){
      std::cout << envp[k] << std::endl;
      }
   }
   
   std::exit( 0 );
}
